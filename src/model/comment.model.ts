import { Expose } from 'class-transformer'
import { IsString, IsBoolean, IsNotEmpty, IsOptional} from 'class-validator'
import {Menssage} from '../config/menssages/default-menssages'

export class CommentModel {
  @Expose({name: 'id'})
  @IsString({message: Menssage.isString})
  @IsOptional()
  _id?: string
  
  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  body!: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  author_id!: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsOptional()
  comment_id!: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  post_id!: string
}