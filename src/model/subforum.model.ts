import { Expose } from 'class-transformer'
import { IsString, IsBoolean, IsNotEmpty, IsOptional} from 'class-validator'
import {Menssage} from '../config/menssages/default-menssages'

export class SubForumModel {
  @Expose({name: 'id'})
  @IsString({message: Menssage.isString})
  @IsOptional()
  _id?: string
  
  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  name!: string
  
  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  forum_id?: string
}