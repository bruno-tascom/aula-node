import { SeedForum, UnseedForum } from "./forum.seed"

/** Busca os argumentos passados no terminal */
const args = process.argv.slice(2)
const seedTypes = args[0]
const seed = args[1]

/** Script para executar a seed */
switch (`${seedTypes}:${seed}`) {
    case 'seed:Forum':
        SeedForum()
        break;
    case 'unseed:Forum':
        UnseedForum()
        break;
    default:
        console.log(`Nenhuma seed foi encontrada com o nome de ${`${seedTypes}:${seed}`}`)
        break;
}