import { ForumRepository } from "../../../schema/forum.schema"
import { connection } from "../connection"
import { ForumMock } from "../../../mock/index.mock" 

/** Função para realizar a seed */
export const SeedForum = async () => {
    try {
        console.log(`Iniciando a seed de forum...`)
        await connection()
        /** Cria com os dados do mock */
        await ForumRepository.insertMany(ForumMock)
        console.log(`Dados da sedd de forum salvos!`)
    } catch (error) {
        console.log(`Erro ao fazer a seed de forum: ${error}`)
    } finally {
        console.log(`Fim da sedd de forum`)
        process.exit(0)
    }
}

/** Função para desfazer a seed */
export const UnseedForum = async () => {
    try {
        console.log(`Iniciando a deleção da collection de forum...`)
        await connection()
        /** Deleta tudo na tabela */
        await ForumRepository.deleteMany({})
        console.log(`Dados da collection de forum foram excluídos!`)
    } catch (error) {
        console.log(`Erro ao fazer deletar a collection de forum: ${error}`)
    } finally {
        console.log(`Fim da deleção de forum`)
        process.exit(0)
    }
}