import { Schema, model } from 'mongoose'
import { Menssage } from '../config/menssages/default-menssages'

const PostSchema = new Schema({
  /** Título do post */
  title: {
    type: String,
    required: [true, Menssage.mongoRequired('título')],
    uppercase: true
  },

  /** Descrição do post */
  body: {
    type: String,
    required: [true, Menssage.mongoRequired('corpo')],
    uppercase: true
  },

  /** Forum do post */
  forum_id: {
    type: Schema.Types.ObjectId,
    ref: 'Forum',
    required: [true, Menssage.mongoRequired('forum')],
  },

  /** Subforum do post */
  subforum_id: {
    type: Schema.Types.ObjectId,
    ref: 'SubForum',
  },

  /** Usuário que criou o post */
  author_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: [true, Menssage.mongoRequired('usuário')],
  },

  /** Caso o post seja fixado */
  is_pinned: {
    type: Boolean,
    default: false,
  },

  /** Caso o post esteja bloqueado */
  is_locked: {
    type: Boolean,
    default: false,
  },
}, {
  strict: true,
  timestamps: true,
  versionKey: false
})

export const PostRepository = model('Post', PostSchema)
PostRepository.syncIndexes()
