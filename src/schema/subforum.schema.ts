import { Schema, model } from 'mongoose'
import { Menssage } from '../config/menssages/default-menssages'

const SubForumSchema = new Schema({
  /** Nome do subforum */
  name: {
    type: String,
    required: [true, Menssage.mongoRequired('nome')],
    uppercase: true
  },

  /** Forum pelo o qual pertence o subforum */
  forum_id: {
    type: Schema.Types.ObjectId,
    ref: 'Forum',
    required: [true, Menssage.mongoRequired('forum')],
  },
}, {
  strict: true,
  timestamps: true,
  versionKey: false
})

/** Index */
/** Não deixa criar um subforum com o mesmo nome para o mesmo forum */
SubForumSchema.index({name: 1, forum_id: 1}, {unique: true})

/** Middleware */

export const SubForumRepository = model('SubForum', SubForumSchema)
SubForumRepository.syncIndexes()
