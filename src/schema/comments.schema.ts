import { Schema, model } from 'mongoose'
import { Menssage } from '../config/menssages/default-menssages'

const CommentsSchema = new Schema({
  /** Comentário */
  body: {
    type: String,
    required: [true, Menssage.mongoRequired('nome')],
    uppercase: true
  },

  /** Usuário que fez o comentário */
  author_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: [true, Menssage.mongoRequired('usuário')],
  },

  /** Esse campo é para fazer uma resposta a um comentário */
  comment_id: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
    required: [true, Menssage.mongoRequired('comentário')],
  },

  /** O post a qual o comentário faz parte */
  post_id: {
    type: Schema.Types.ObjectId,
    ref: 'Post',
    required: [true, Menssage.mongoRequired('poste')],
  },
  
}, {
  strict: true,
  timestamps: true,
  versionKey: false
})

/** Index */

/** Middleware */

export const CommentsRepository = model('SubForum', CommentsSchema)
CommentsRepository.syncIndexes()
