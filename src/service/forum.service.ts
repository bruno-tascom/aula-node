import { ForumRepository } from "../schema/forum.schema"

export const listAllForumPostsService = async (per_page: string, page: string) => {
  const forums = await ForumRepository.aggregate([
    {
      $lookup: {
        from: 'posts',
        as: 'posts',
        localField: '_id',
        foreignField: 'forum_id'
      }
    },
    {
      $limit: Number(per_page)
    },
    {
      $skip: Number(per_page) * (Number(page) - 1)
    }
])
  
  // const forumsResult = forums.map((forum) => {
  //   return {
  //     ...forum.toJSON(),
  //     posts: []
  //   }
  // })

  return {
    forums: forums,
    pinned_posts: [],
    per_page,
    page
  }
}