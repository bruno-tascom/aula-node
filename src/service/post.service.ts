import { PostModel } from "../model/post.model";
import { PostRepository } from "../schema/post.schema";

export const createPostService = async (post: PostModel) => {
  return await PostRepository.create(post)
}