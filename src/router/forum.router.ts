import { Router } from 'express'
import { ForumController } from '../controller/index.controller'

export const forumRouter = Router()

forumRouter.get('/forum', [], ForumController.listAllForumPostsController)