import { Router } from "express"
import {userRouter, userRouterV2} from "./user.router"
import { authRouter } from "./auth.router"
import { forumRouter } from './forum.router'
import { postRouter } from './post.router'

const routers = Router()

routers.use('/v1', authRouter, userRouter, forumRouter, postRouter)
routers.use('/v2', userRouterV2)

export {
  routers
}
