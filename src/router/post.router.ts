import { Router } from 'express'
import { PostController } from '../controller/index.controller'
import { authenticate } from '../config/authentication/strategy.passport'

export const postRouter = Router()

postRouter.post('/post', [authenticate()], PostController.createPostController)