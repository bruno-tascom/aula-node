import {Request, Response} from 'express'
import { PostService } from '../service/index.service'

export const createPostController = async (req: Request, res: Response) => {
  const body = req.body
  const post = await PostService.createPostService(body)
  res.status(201).json(post)
}