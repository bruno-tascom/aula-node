import * as UserController from './user.controller'
import * as AuthController from './auth.controller'
import * as ForumController from './forum.controller'
import * as PostController from './post.controller'

export {
  UserController,
  AuthController,
  ForumController,
  PostController
}