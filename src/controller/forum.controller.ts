import { Request, Response } from 'express'
import { ForumService } from '../service/index.service'

export const listAllForumPostsController = async (req: Request, res: Response) =>{
  const { per_page, page } = req.query
  const forum = await ForumService.listAllForumPostsService(String(per_page), String(page))
  res.status(200).json(forum)
}